<?php

namespace Tests\Functional;

class AppControllerTest extends BaseTestCase
{
    private static function cleanDB() {
        file_put_contents(__DIR__ . '/../../data/db.json', json_encode([
            'purchases' => [
                [
                    'purchaseDate' => '2016-01-01',
                    'quantity' => 200,
                    'itemPrice' => 10
                ],
                [
                    'purchaseDate' => '2016-01-05',
                    'quantity' => 250,
                    'itemPrice' => 15
                ],
                [
                    'purchaseDate' => '2016-01-10',
                    'quantity' => 150,
                    'itemPrice' => 12.5
                ]
            ],
            'sales' => [
                [
                    'soldDate' => '2016-01-03',
                    'quantity' => 50
                ],
                [
                    'soldDate' => '2016-01-08',
                    'quantity' => 225
                ],
                [
                    'soldDate' => '2016-01-11',
                    'quantity' => 50
                ]
            ]
        ]));
    }

    public static function setUpBeforeClass()
    {
        self::cleanDB();
    }

    protected function tearDown()
    {
        self::cleanDB();
    }

    /**
     * Test that the view route does not allow POST requests
     */
    public function testPostViewRouteNotAllowed()
    {
        $response = $this->runApp('POST', '/api/view');

        $this->assertEquals(405, $response->getStatusCode());
        $this->assertContains('Method not allowed', (string) $response->getBody());
    }

    /**
     * Test that the view route returns a status code 200 with correct data in the returned JSON
     */
    public function testGetViewRouteWithCorrectData()
    {
        $response = $this->runApp('GET', '/api/view');
        $obj = json_decode($response->getBody());

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertEquals(3, count($obj->data->purchases));
        $this->assertEquals(3, count($obj->data->sales));

        $this->assertEquals('2016-01-01', $obj->data->purchases[0]->purchaseDate);
        $this->assertEquals(200, $obj->data->purchases[0]->quantity);
        $this->assertEquals(10, $obj->data->purchases[0]->itemPrice);

        $this->assertEquals('2016-01-05', $obj->data->purchases[1]->purchaseDate);
        $this->assertEquals(250, $obj->data->purchases[1]->quantity);
        $this->assertEquals(15, $obj->data->purchases[1]->itemPrice);

        $this->assertEquals('2016-01-10', $obj->data->purchases[2]->purchaseDate);
        $this->assertEquals(150, $obj->data->purchases[2]->quantity);
        $this->assertEquals(12.5, $obj->data->purchases[2]->itemPrice);
    }

    /**
     * Test that the stats route returns a status code 404 when no date parameter is given
     */
    public function testGetStatsRouteWithoutDateParameter()
    {
        $response = $this->runApp('GET', '/api/stats');

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertContains('Page Not Found', (string) $response->getBody());
    }

    /**
     * Test that the stats route returns a status code 200 with correct data in the returned JSON
     */
    public function testGetStatsRouteWithDateParameter()
    {
        $response = $this->runApp('GET', '/api/stats/2016-01-11');
        $obj = json_decode($response->getBody());

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertEquals(275, $obj->data->stockCount);
        $this->assertEquals(3750, $obj->data->totalValue);
        $this->assertEquals(3875, $obj->data->soldValue);

        $response = $this->runApp('GET', '/api/stats/2016-01-08');
        $obj = json_decode($response->getBody());

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertEquals(175, $obj->data->stockCount);
        $this->assertEquals(2625, $obj->data->totalValue);
        $this->assertEquals(3125, $obj->data->soldValue);
    }

    /**
     * Test that the buy route returns a status code 400 when the parameter 'purchasedDate' is not supplied
     */
    public function testPostBuyRouteWithoutPurchaseDateParameter()
    {
        $response = $this->runApp('POST', '/api/buy');
        $obj = json_decode($response->getBody());

        $this->assertEquals(400, $response->getStatusCode());

        $this->assertObjectHasAttribute('error', $obj);
        $this->assertEquals(400, $obj->error->code);
        $this->assertEquals('\'purchaseDate\' parameter not present.', $obj->error->message);
    }

    /**
     * Test that the buy route returns a status code 400 when the parameter 'quantity' is not supplied
     */
    public function testPostBuyRouteWithoutQuantityParameter()
    {
        $response = $this->runApp('POST', '/api/buy', [
            'purchaseDate' => '2018-11-18'
        ]);
        $obj = json_decode($response->getBody());

        $this->assertEquals(400, $response->getStatusCode());

        $this->assertObjectHasAttribute('error', $obj);
        $this->assertEquals(400, $obj->error->code);
        $this->assertEquals('\'quantity\' parameter not present.', $obj->error->message);
    }

    /**
     * Test that the buy route returns a status code 400 when the parameter 'itemPrice' is not supplied
     */
    public function testPostBuyRouteWithoutItemPriceParameter()
    {
        $response = $this->runApp('POST', '/api/buy', [
            'purchaseDate' => '2018-11-18',
            'quantity' => 100
        ]);
        $obj = json_decode($response->getBody());

        $this->assertEquals(400, $response->getStatusCode());

        $this->assertObjectHasAttribute('error', $obj);
        $this->assertEquals(400, $obj->error->code);
        $this->assertEquals('\'itemPrice\' parameter not present.', $obj->error->message);
    }

    /**
     * Test that the buy route returns a status code 200 with correct data in the returned JSON
     */
    public function testPostBuyRouteWithCorrectData()
    {
        $response = $this->runApp('POST', '/api/buy', [
            'purchaseDate' => '2018-11-18',
            'quantity' => 100,
            'itemPrice' => 15
        ]);
        $obj = json_decode($response->getBody());

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertEquals('2018-11-18', $obj->data->purchaseDate);
        $this->assertEquals(100, $obj->data->quantity);
        $this->assertEquals(15, $obj->data->itemPrice);
    }

    /**
     * Test that the sell route returns a status code 400 when the parameter 'soldDate' is not supplied
     */
    public function testPostSellRouteWithoutSoldDateParameter()
    {
        $response = $this->runApp('POST', '/api/sell');
        $obj = json_decode($response->getBody());

        $this->assertEquals(400, $response->getStatusCode());

        $this->assertObjectHasAttribute('error', $obj);
        $this->assertEquals(400, $obj->error->code);
        $this->assertEquals('\'soldDate\' parameter not present.', $obj->error->message);
    }

    /**
     * Test that the sell route returns a status code 400 when the parameter 'quantity' is not supplied
     */
    public function testPostSellRouteWithoutQuantityParameter()
    {
        $response = $this->runApp('POST', '/api/sell', [
            'soldDate' => '2018-11-18'
        ]);
        $obj = json_decode($response->getBody());

        $this->assertEquals(400, $response->getStatusCode());

        $this->assertObjectHasAttribute('error', $obj);
        $this->assertEquals(400, $obj->error->code);
        $this->assertEquals('\'quantity\' parameter not present.', $obj->error->message);
    }

    /**
     * Test that the sell route returns a status code 200 with correct data in the returned JSON
     */
    public function testPostSellRouteWithCorrectData()
    {
        $response = $this->runApp('POST', '/api/sell', [
            'soldDate' => '2018-11-18',
            'quantity' => 100
        ]);
        $obj = json_decode($response->getBody());

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertEquals('2018-11-18', $obj->data->soldDate);
        $this->assertEquals(100, $obj->data->quantity);
    }
}