# FIFO Inventory REST API

This is a RESTful API that implements basic inventory functionality. Calculations done on the inventory are based on the [FIFO (First In, First Out) principle](https://en.wikipedia.org/wiki/FIFO_(computing_and_electronics)).

## Installation

Setting up the application is pretty simple. All you need to have is a recent PHP version and Composer installed (and Docker as well). Composer will be used to install dependencies needed for the application to run, and it will make it a lot easier to run the test suite.

Install the Composer dependencies after cloning the repository:

```bash
composer install
```

Run the test suite to make sure everything is working:

```bash
composer test
```

To run the application itself:

```bash
composer start
```

You can also run it with Docker:

```bash
docker-compose up
```

The application will start listening on port 8080.

## API documentation

Sadly, I did not have a lot of time to make a nice API documentation, so I will just briefly describe the endpoints below.

**View database contents:** `GET /api/view`

**Statistical information:** `GET /api/stats/{date}` - *{date} should be in Y-m-d format*

**Purchase item:** `POST /api/buy`

```json
{
    "purchaseDate": "2018-11-17",
    "quantity": 100,
    "itemPrice": 15
}
```

**Sell item:** `POST /api/sell`

```json
{
    "soldDate": "2018-11-17",
    "quantity": 50
}
```