<?php

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Created by PhpStorm.
 * User: fredmark
 * Date: 17/11/2018
 * Time: 20.18
 */

class AppController
{
    private static function readDB() {
        return json_decode(file_get_contents(__DIR__ . '/../../data/db.json'));
    }

    private static function writeDB($db) {
        file_put_contents(__DIR__ . '/../../data/db.json', json_encode($db));
    }

    public static function viewAction(Request $request, Response $response) {
        return $response->withJson([
            'data' => self::readDB()
        ]);
    }

    public static function statsAction(Request $request, Response $response, array $args) {
        if (is_null($args['date'])) {
            return $response->withJson([
                'error' => [
                    'code' => 400,
                    'message' => '\'date\' parameter not present.'
                ]
            ], 400);
        }

        $db = self::readDB();
        $purchases = $db->purchases;
        $sales = $db->sales;

        $stockQtyCount = 0;
        $saleQtyCount = 0;

        $totalValue = 0;
        $soldValue = 0;

        foreach ($sales as $sale) {
            if ($sale->soldDate > $args['date'])
                break;

            $stockQtyCount -= $sale->quantity;
            $saleQtyCount += $sale->quantity;
        }

        /**
         * Here is where the exciting stuff is :-)
         */
        foreach ($purchases as $purchase) {
            if ($purchase->purchaseDate > $args['date'])
                break;

            $stockQtyCount += $purchase->quantity;

            if ($purchase->quantity > $saleQtyCount) {
                $qtyLeft = $purchase->quantity - $saleQtyCount;

                if ($qtyLeft > 0) {
                    $totalValue += $qtyLeft * $purchase->itemPrice;
                    $soldValue += $saleQtyCount * $purchase->itemPrice;
                    $saleQtyCount = 0;
                }
            } else {
                $saleQtyCount -= $purchase->quantity;
                $soldValue += $purchase->quantity * $purchase->itemPrice;
            }
        }

        return $response->withJson([
            'data' => [
                'stockCount' => $stockQtyCount,
                'totalValue' => $totalValue,
                'soldValue' => $soldValue
            ]
        ]);
    }

    public static function buyAction(Request $request, Response $response) {
        $data = $request->getParsedBody();

        if (is_null($data['purchaseDate'])) {
            return $response->withJson([
                'error' => [
                    'code' => 400,
                    'message' => '\'purchaseDate\' parameter not present.'
                ]
            ], 400);
        }

        if (is_null($data['quantity'])) {
            return $response->withJson([
                'error' => [
                    'code' => 400,
                    'message' => '\'quantity\' parameter not present.'
                ]
            ], 400);
        }

        if (is_null($data['itemPrice'])) {
            return $response->withJson([
                'error' => [
                    'code' => 400,
                    'message' => '\'itemPrice\' parameter not present.'
                ]
            ], 400);
        }

        $db = self::readDB();

        $purchase = [
            'purchaseDate' => $data['purchaseDate'],
            'quantity' => $data['quantity'],
            'itemPrice' => $data['itemPrice']
        ];

        $db->purchases[] = $purchase;
        self::writeDB($db);

        return $response->withJson([
            'data' => $purchase
        ]);
    }

    public static function sellAction(Request $request, Response $response) {
        $data = $request->getParsedBody();

        if (is_null($data['soldDate'])) {
            return $response->withJson([
                'error' => [
                    'code' => 400,
                    'message' => '\'soldDate\' parameter not present.'
                ]
            ], 400);
        }

        if (is_null($data['quantity'])) {
            return $response->withJson([
                'error' => [
                    'code' => 400,
                    'message' => '\'quantity\' parameter not present.'
                ]
            ], 400);
        }

        $db = self::readDB();

        $sale = [
            'soldDate' => $data['soldDate'],
            'quantity' => $data['quantity']
        ];

        $db->sales[] = $sale;
        self::writeDB($db);

        return $response->withJson([
            'data' => $sale
        ]);
    }
}