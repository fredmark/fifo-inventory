<?php

require_once 'controllers/AppController.php';

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->group('/api', function() {
    $this->get('/view', function (Request $request, Response $response) {
        return AppController::viewAction($request, $response);
    });

    $this->get('/stats/{date}', function (Request $request, Response $response, array $args) {
        return AppController::statsAction($request, $response, $args);
    });

    $this->post('/buy', function (Request $request, Response $response) {
        return AppController::buyAction($request, $response);
    });

    $this->post('/sell', function (Request $request, Response $response) {
        return AppController::sellAction($request, $response);
    });
});
